﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    private GameObject Player;
    public GameObject EnemyPrefab;
    // Start is called before the first frame update
    void Start()
    {
        // Player = FindObjectOfType<Player>().gameObject;
        //Player = GameObject.Find("Player");
        //Player = GameObject.FindGameObjectWithTag("Player");
    }
    void OnSpawn()
    {
        GameObject enemy = Instantiate(EnemyPrefab);
        enemy.GetComponent<Ai>().target = Player.transform;

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
