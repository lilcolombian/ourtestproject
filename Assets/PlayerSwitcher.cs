﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSwitcher : MonoBehaviour
{
    private List<GameObject> heros;
    GameObject alpha;
    public GameObject CurrentHero;
    // Start is called before the first frame update
    void Start()
    {
        //    alpha = transform.GetChild(0).gameObject;
        string nameTOcompare= "Alpha";
        for (int i = 0; i < transform.childCount; i++)
        {
            if(transform.GetChild(i).name== nameTOcompare)
            {
                CurrentHero = transform.GetChild(i).gameObject;
            }
        }
        SwitchToHero(0);

    }
    public void SwitchToHero(int i)
    {
        if (i >= heros.Count)
        {
            return;
            //error
        }

        if (CurrentHero != null)
            CurrentHero.SetActive(false);
        CurrentHero = heros[i];
        CurrentHero.SetActive(true);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
