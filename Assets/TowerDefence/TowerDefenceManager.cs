﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerDefenceManager : MonoBehaviour
{
    [SerializeField]// to show in inspector
    private  GameObject AITargetLocation;
    public GameObject AiSpawnPoint;
    private int NumberofEnemiesPerRound;
    private int EnemiesLeft;
    private int delayBetweenEnemies;
    public GameObject AIPrefab;
    public GameObject RejectionUI; 
    public GameObject SettingUI;
    public GameObject LoseUi;

    public static TowerDefenceManager Instance;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if(Instance != null && Instance != this)// there is another version of this class in the scene
        {
            Destroy(this);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetEnemiesNumber(string  input)
    {
        NumberofEnemiesPerRound = Int32.Parse( input);

    }
    
    public void GetEnemiesSpawnDelay(string  input)
    {
       delayBetweenEnemies= Int32.Parse( input);

    }

    public void StartRound()
    {
        if (NumberofEnemiesPerRound > 0&& delayBetweenEnemies>=0)
        {
            SettingUI.SetActive(false);
           StartCoroutine(SpawnObjects());
        }
        else
        {
            RejectionUI.SetActive(true);
        }
    }
    public void OnAIKilled()
    {
        EnemiesLeft--;
        if (EnemiesLeft <= 0)
        {
            //round over
        }
    }
    public void EnemyArrived()
    {
        //enemy reached target
        Debug.Log("you Lose");
        LoseUi.SetActive(true);
    }
    public IEnumerator SpawnObjects()
    {
        for (int i = 0; i < NumberofEnemiesPerRound; i++)// numb of children
        {
            GameObject AiMOB = Instantiate(AIPrefab, AiSpawnPoint.transform.position, AiSpawnPoint.transform.rotation);
          
            AiMOB.GetComponent<Ai>().SetTarget(AITargetLocation.transform);
            yield return new WaitForSeconds(delayBetweenEnemies);
        }
        EnemiesLeft = NumberofEnemiesPerRound;
    }
}
