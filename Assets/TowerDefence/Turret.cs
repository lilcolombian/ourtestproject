﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour
{
    public List<GameObject> PossibleTargets = new List<GameObject>();

    private GameObject TurretTarget;
    public float RateOfFire;
    float firetimer;
    public float Damage;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (TurretTarget)
        {
            firetimer += Time.deltaTime;
            transform.LookAt(TurretTarget.transform.position);
            if (firetimer >= RateOfFire)
            {
                firetimer = 0;
                //fire 
                print("fire");
                if (TurretTarget.GetComponent<Ai>().didHitKill(Damage))
                {
                    if (PossibleTargets.Count > 0)
                    {
                        TurretTarget = PossibleTargets[0];
                        PossibleTargets.RemoveAt(0);
                    }
                }
            }

        }

    }
    public void ClearTarget(GameObject target)
    {
        if (target == TurretTarget)
        {
            TurretTarget = null;
            if(PossibleTargets.Count > 1)
            {
                TurretTarget = PossibleTargets[0];
                PossibleTargets.RemoveAt(0);
            }
        }
        else// target is not TurretTarget
            RemovePossibleTarget(target);
    }
    public void SetTarget(GameObject target)
    {
        TurretTarget = target;

    }
    public bool HasTarget()
    {
        if (TurretTarget != null)
            return true;
        return false;
    }

    public void AddPossibleTarget(GameObject possibleTarget)
    {
        PossibleTargets.Add(possibleTarget);
    }
    public void RemovePossibleTarget(GameObject targetToRemove)
    {
        if (PossibleTargets.Contains(targetToRemove))
            PossibleTargets.Remove(targetToRemove);
        else
            Debug.LogError("tried to remove a target that wasnt registered");
    }


}
