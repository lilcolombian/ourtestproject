﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class AIController : MonoBehaviour
{
    public Transform[] TestTransforms;
    public int patrolIndex = 0;
    public float MinDistBeforeTurn = .1f;
    NavMeshAgent myAgent;
    // Start is called before the first frame update
    void Start()
    {
        myAgent = GetComponent<NavMeshAgent>();
        myAgent.SetDestination(TestTransforms[0].position);
    }

    // Update is called once per frame
    void Update()
    {
        if (myAgent.remainingDistance < MinDistBeforeTurn)
        {
            patrolIndex++;
            if (patrolIndex == TestTransforms.Length)
                patrolIndex = 0;
            myAgent.SetDestination(TestTransforms[patrolIndex].position);
        }
    }
}
