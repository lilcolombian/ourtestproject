﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Gun
{
    public int pelletCount;
    public float bulletSpread;


    public override void Shoot()
    {
        if (CDtime <= 0)
        {
            CDtime = ROF;
            for (int i = 0; i < pelletCount; i++)
            {
                Instantiate(Bullet, BarrelLoc.transform.position +( new Vector3(0, 0, bulletSpread * i)), BarrelLoc.transform.rotation);
            }
        }

    }
}
