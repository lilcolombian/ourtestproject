﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyRaycaster : MonoBehaviour
{
    RaycastHit hit;
    Vector3 tar;
    public LayerMask layer;
    public float damage;
    // Start is called before the first frame update
    void Start()
    {

    }



    // Update is called once per frame
    void Update()
    {
   
            if (Physics.Raycast(transform.position, transform.forward, out hit,Mathf.Infinity,layer))
            {
                print(hit.collider.gameObject);
                Debug.DrawLine(transform.position, hit.point, Color.red);
                if (hit.collider.gameObject.GetComponent<Character>())
                {
                    print("Hit Player");
                  //  Character player = hit.collider.gameObject.GetComponent<Character>();
                //    player.TakeDamage(damage, false);
                }

            }
            else
            {
                Debug.DrawLine(transform.position, transform.forward * 100, Color.blue);

                print("DID NOT HIT ANYTHING");
            }
        
    }
}
