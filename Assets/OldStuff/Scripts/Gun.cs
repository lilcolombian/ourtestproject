﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public GameObject Bullet;
    public GameObject BarrelLoc;
    public float ROF;
    [HideInInspector]
    public float CDtime;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (CDtime <= 0)
        {

        }
        else
        {
            CDtime -= Time.deltaTime;
        }
    }

    public virtual void Shoot()
    {
        if (CDtime <= 0)
        {
            CDtime = ROF;

            Instantiate(Bullet, BarrelLoc.transform.position, BarrelLoc.transform.rotation);

        }
    }

}
