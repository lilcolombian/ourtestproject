﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackManager : MonoBehaviour
{
    public static AttackManager Instance;

    public Character Hero;
    public Character Villian;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if(Instance != this)
        {
            Destroy(this);
        }
    }

    public void ProcessAttack()
    {
        float damageToDeliver = Hero.GetDamage();
        damageToDeliver -= Villian.GetDefence();
        Villian.TakeDamage(damageToDeliver);
    }
}
