﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class MyOtherClass:MonoBehaviour
{
    public Character playerCharacter;
    public TextMeshProUGUI NameText;
    public TextMeshProUGUI HealthText;
    public bool testBool;

    private void Start()
    {
        int total=0;

        for (int i = 0; i <= 10; i++)

        {

            for (int j = 0; j <= 5; j++)

            {

                total += j;

            }

        }

        print(total);
    }
    private void Update()
    {
        
    }

    public void SetCharacter()
    {
        playerCharacter.SetName("John");
        playerCharacter.SetHealth(100, 1);
        UpdateInfo();
        
       }
    public void UpdateInfo()
    {
        NameText.text = playerCharacter.GetName();
        HealthText.text = "HP: " + playerCharacter.GetHealth().ToString();

    }

}
