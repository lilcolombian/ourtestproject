﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSController : MonoBehaviour
{
    public float sensitivityX = 15F;
    public float sensitivityY = 15F;

    public float minimumX = -360F;
    public float maximumX = 360F;

    public float minimumY = -60F;
    public float maximumY = 60F;

    float rotationY = 0F;

    public bool Inverted;

    public Gun gunEquipped;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RotateChar();
        InputLogic();
    }
    void InputLogic() {

        //Player Shooting
        if (Input.GetMouseButtonDown(0)){
            gunEquipped.Shoot();
        
        }
    
    }

    void RotateChar()
    {

        //FPS CAMERA Rotation 
        //X rotaion based on current 
        float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;


        rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
        //set limits for Y axis speed 
        rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);
        if (Inverted)
            rotationY = -rotationY;

        //apply roation to character
        transform.localEulerAngles = new Vector3(0, rotationX, rotationY);


    }

}
