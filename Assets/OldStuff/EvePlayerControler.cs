﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvePlayerControler : MonoBehaviour
{

    public float FowardMovement = 0;
    public float JumpPower;
    public Vector3 JumpDir;
    public LayerMask mask;
    public float MinDistFromFloor;
    public float MinAirDistance;
    public float distFromFloor;
    Rigidbody rigidbody;

    Animator OurAnimator;
    float fowardAmount;
    bool DidPressJump;
    bool Jumping;
    bool InAir;
   

    RaycastHit hit;
    // Start is called before the first frame update
    void Start()
    {
        OurAnimator = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Jump();
        CheckAirTime();
    }

    void CheckAirTime()
    {
        if (Physics.Raycast(transform.position, transform.position - transform.up, out hit, Mathf.Infinity, mask))
        {
            distFromFloor = Vector3.Distance(transform.position, hit.point);
            if (Jumping && !InAir)
            {
                if (distFromFloor > MinAirDistance)
                {
                    InAir = true;
                }
            }
            print(hit.collider);

            if (distFromFloor < MinDistFromFloor && InAir)
            {
                if (OurAnimator.GetBool("isJumping"))
                {
                    Jumping = false;
                    Debug.Log("Time to not jump");
                    InAir = false;
                    OurAnimator.SetBool("isJumping", false);
                }
            }
        }
        else
        {
            Debug.DrawRay(transform.position, transform.position - transform.up, Color.red, 10f);
        }
    }

    public void Move()
    {
        fowardAmount = Input.GetAxis("Vertical");
        OurAnimator.SetFloat("Movement", fowardAmount);
       
        
    }
    public void Jump()
    {
        DidPressJump = Input.GetKeyDown(KeyCode.Space);
        print(fowardAmount + " did press jump:" + DidPressJump);
        if (DidPressJump && !OurAnimator.GetBool("isJumping"))
        {
            Jumping = true;
            InAir = false;
            OurAnimator.SetBool("isJumping", true);
            rigidbody.AddForce(JumpDir * JumpPower, ForceMode.Impulse);
        }
    }


}
